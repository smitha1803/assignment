package se.ericsson.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.ericsson.exception.InvalidInputException;


public class TriangleTest {
	private Triangle sssTriangle;
	private Triangle sasTriangle;
	private Triangle invalidSSSTriangle;
	private Triangle invalidSASTriangle;
	private static final double DELTA = 1e-8;

	@Before
	public void init() throws Exception {
		
		sssTriangle = new Triangle(7, 6, 5, false);
		sasTriangle = new Triangle(3, 4, 67, true);

	}
	
	@After
	public void cleanup(){
		sssTriangle = null;
		sasTriangle=null;
		invalidSSSTriangle = null;
		invalidSASTriangle=null;
	}


	/**
	 * Test for remaining parameter of a SSS Triangle
	 */
	@Test
	public void testSSSTriangle() {
		sssTriangle.computeRemainingTriangleParameters();
		assertEquals(78.46304096718453, sssTriangle.getAngle_a(), DELTA);
		assertEquals(57.12165043562251, sssTriangle.getAngle_b(), DELTA);
		assertEquals(44.415308597192976, sssTriangle.getAngle_c(), DELTA);
		assertEquals(14.696938456699069, sssTriangle.getArea(), DELTA);
		assertEquals(18, sssTriangle.getPerimeter(), DELTA);
	}

	/**
	 * Test for remaining parameter of a SAS Triangle
	 */
	@Test
	public void testSASTriangle() {
		sasTriangle.computeRemainingTriangleParameters();
		assertEquals(44.320476187694226, sasTriangle.getAngle_a(), DELTA);
		assertEquals(68.67952381230577, sasTriangle.getAngle_b(), DELTA);
		assertEquals(3.952524878638644, sasTriangle.getSide_c(), DELTA);
		assertEquals(5.523029120714644, sasTriangle.getArea(), DELTA);
		assertEquals(10.95252487863864, sasTriangle.getPerimeter(), DELTA);
	}

	@Test
	public void testInvalidInputExceptionforSSSTriangle() {
		boolean thrown = false;
		try {
			invalidSSSTriangle = new Triangle(9, 1, 3, false);
		} catch (InvalidInputException e) {
			thrown = true;
		}
		assertTrue(thrown);
	}

	@Test
	public void testInvalidInputExceptionforSASTriangle() {
		boolean thrown = false;
		try {
			invalidSASTriangle = new Triangle(9, 1, 185, true);
		} catch (InvalidInputException e) {
			thrown = true;
		}
		assertTrue(thrown);
	}

}
