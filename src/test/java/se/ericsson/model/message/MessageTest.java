package se.ericsson.model.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.ericsson.model.Parameters;

public class MessageTest {
	private Message message;
	private static final double DELTA = 1e-8;

	@Before
	public void init() throws Exception {

		message = new Message();
	}

	@After
	public void cleanup() throws Exception {

		message = null;
	}

	@Test
	public void testMessage() throws Exception {
		
		//testing getters & setters
		message.setAngle_a(78.46304096718453);
		message.setAngle_b(57.12165043562251);
		message.setAngle_c(44.415308597192976);
		message.setSide_a(7);
		message.setSide_b(6);
		message.setSide_c(5);
		message.setArea(14.696938456699069);
		message.setPerimeter(18);
		message.setSASTriangle(true);

		assertEquals(78.46304096718453, message.getAngle_a(), DELTA);
		assertEquals(57.12165043562251, message.getAngle_b(), DELTA);
		assertEquals(44.415308597192976, message.getAngle_c(), DELTA);
		assertEquals(7, message.getSide_a(), DELTA);
		assertEquals(6, message.getSide_b(), DELTA);
		assertEquals(5, message.getSide_c(), DELTA);
		assertEquals(18, message.getPerimeter(), DELTA);
		assertTrue(message.isSASTriangle());
		
		//testing parser methods

		byte[] array = message.toByteArray();
		Message message2 = Message.parseFrom(array);
		assertEquals(message.getAngle_a(), message2.getAngle_a(), DELTA);
		
		
		//testing formattedOutput method
		
		DecimalFormat df = new DecimalFormat("#,###,##0.000");
		StringBuffer sb1 = new StringBuffer();

		sb1.append(Message.OUTPUT);
		sb1.append(Message.NEWLINE);
		sb1.append(Parameters.AREA + ": " + df.format(message.getArea()) + " " + Message.SQ_UNITS);
		sb1.append(Message.NEWLINE);
		sb1.append(Parameters.PERIMETER + ":" + df.format(message.getPerimeter())+ " "+Message.UNITS);
		sb1.append(Message.NEWLINE);
		sb1.append(Parameters.ANGLE_A + ": " + df.format(message.getAngle_a())+ " "+Message.DEGREES);
		sb1.append(Message.NEWLINE);
		sb1.append(Parameters.ANGLE_B + ": " + df.format(message.getAngle_b())+ " "+Message.DEGREES);
		sb1.append(Message.NEWLINE);
		if (!message.isSASTriangle())
			sb1.append(Parameters.ANGLE_C + ": " + df.format(message.getAngle_c())+" "+Message.DEGREES);
		else {
			sb1.append(Parameters.SIDE_C + ": " + df.format(message.getSide_c())+ " "+Message.UNITS);
		}
		sb1.append(Message.NEWLINE);

		
		String output = message.getFormattedOutput();
		assertEquals("", sb1.toString(), output);
		
		message2.setSASTriangle(false);
		StringBuffer sb2 = new StringBuffer();

		sb2.append(Message.OUTPUT);
		sb2.append(Message.NEWLINE);
		sb2.append(Parameters.AREA + ": " + df.format(message2.getArea()) + " " + Message.SQ_UNITS);
		sb2.append(Message.NEWLINE);
		sb2.append(Parameters.PERIMETER + ":" + df.format(message2.getPerimeter())+ " "+Message.UNITS);
		sb2.append(Message.NEWLINE);
		sb2.append(Parameters.ANGLE_A + ": " + df.format(message2.getAngle_a())+ " "+Message.DEGREES);
		sb2.append(Message.NEWLINE);
		sb2.append(Parameters.ANGLE_B + ": " + df.format(message2.getAngle_b())+ " "+Message.DEGREES);
		sb2.append(Message.NEWLINE);
		if (!message2.isSASTriangle())
			sb2.append(Parameters.ANGLE_C + ": " + df.format(message2.getAngle_c())+" "+Message.DEGREES);
		else {
			sb2.append(Parameters.SIDE_C + ": " + df.format(message2.getSide_c())+ " "+Message.UNITS);
		}
		sb2.append(Message.NEWLINE);

		
		String output2 = message2.getFormattedOutput();
		assertEquals("", sb2.toString(), output2);

		
		
		

	}
}
