package se.ericsson.rabbitmq.client;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.ericsson.model.Triangle;
import se.ericsson.model.message.Message;
import se.ericsson.rabbitmq.client.RpcClient;
import static org.junit.Assert.assertEquals;

public class RpcClientTest {
	private RpcClient rpcClient;
	private static final double DELTA = 1e-8;

	@Before
	public void init() {
		rpcClient = new RpcClient();
	}
	
	@After
	public void clean(){
		rpcClient = null;
	}

	@Test
	public void testCreateRequest() throws Exception {
		Triangle sssTriangle = new Triangle(7, 6, 5, false);
		Message sssTriangleMessage = rpcClient.createRequest(sssTriangle);
		assertEquals(sssTriangle.getSide_a(), sssTriangleMessage.getSide_a(),
				DELTA);
		assertEquals(sssTriangle.getSide_b(), sssTriangleMessage.getSide_b(),
				DELTA);
		assertEquals(sssTriangle.getSide_c(), sssTriangleMessage.getSide_c(),
				DELTA);

		Triangle sasTriangle = new Triangle(7, 6, 52, true);
		Message sasTriangleMessage = rpcClient.createRequest(sasTriangle);
		assertEquals(sasTriangle.getSide_a(), sasTriangleMessage.getSide_a(),
				DELTA);
		assertEquals(sasTriangle.getSide_b(), sasTriangleMessage.getSide_b(),
				DELTA);
		assertEquals(sasTriangle.getAngle_c(),
				sasTriangleMessage.getAngle_c(), DELTA);

	}

}
