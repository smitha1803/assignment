package se.ericsson.rabbitmq.server;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.ericsson.exception.InvalidInputException;
import se.ericsson.model.Triangle;
import se.ericsson.model.message.Message;

public class RpcServerTest {
	private RpcServer rpcServer;
	private static final double DELTA = 1e-8;

	@Before
	public void init() throws IOException {
		rpcServer = new RpcServer("test");
	}

	@After
	public void clean() {
		rpcServer = null;
	}

	@Test
	public void testCreateResponse() throws InvalidInputException {

		Triangle SSSTriangle = new Triangle(7, 6, 5, false);
		SSSTriangle.computeRemainingTriangleParameters();

		Message SSSTriangleMessage = rpcServer.createResponse(SSSTriangle);
		assertEquals(SSSTriangle.getAngle_a(), SSSTriangleMessage.getAngle_a(),
				DELTA);
		assertEquals(SSSTriangle.getAngle_b(), SSSTriangleMessage.getAngle_b(),
				DELTA);
		assertEquals(SSSTriangle.getAngle_c(), SSSTriangleMessage.getAngle_c(),
				DELTA);
		assertEquals(SSSTriangle.getArea(), SSSTriangleMessage.getArea(), DELTA);
		assertEquals(SSSTriangle.getPerimeter(),
				SSSTriangleMessage.getPerimeter(), DELTA);

		Triangle SASTriangle = new Triangle(7, 6, 56, true);
		SASTriangle.computeRemainingTriangleParameters();

		Message SASTriangleMessage = rpcServer.createResponse(SASTriangle);
		assertEquals(SASTriangle.getAngle_a(), SASTriangleMessage.getAngle_a(),
				DELTA);
		assertEquals(SASTriangle.getAngle_b(), SASTriangleMessage.getAngle_b(),
				DELTA);
		assertEquals(SASTriangle.getSide_c(), SASTriangleMessage.getSide_c(),
				DELTA);
		assertEquals(SASTriangle.getArea(), SASTriangleMessage.getArea(), DELTA);
		assertEquals(SASTriangle.getPerimeter(),
				SASTriangleMessage.getPerimeter(), DELTA);

	}

	@Test
	public void testToModel() throws Exception {
		Message sssTriangleMessage = new Message();
		sssTriangleMessage.setSide_a(7);
		sssTriangleMessage.setSide_b(6);
		sssTriangleMessage.setSide_c(5);
		sssTriangleMessage.setSASTriangle(false);

		Triangle sssTriangle = rpcServer.toModel(sssTriangleMessage);
		assertEquals(sssTriangleMessage.getSide_a(), sssTriangle.getSide_a(),
				DELTA);
		assertEquals(sssTriangleMessage.getSide_b(), sssTriangle.getSide_b(),
				DELTA);
		assertEquals(sssTriangleMessage.getSide_c(), sssTriangle.getSide_c(),
				DELTA);

		Message sasTriangleMessage = new Message();
		sasTriangleMessage.setSide_a(7);
		sasTriangleMessage.setSide_b(6);
		sasTriangleMessage.setAngle_c(47);
		sasTriangleMessage.setSASTriangle(true);

		Triangle sasTriangle = rpcServer.toModel(sasTriangleMessage);
		assertEquals(sasTriangleMessage.getSide_a(), sasTriangle.getSide_a(),
				DELTA);
		assertEquals(sasTriangleMessage.getSide_b(), sasTriangle.getSide_b(),
				DELTA);
		assertEquals(sasTriangleMessage.getAngle_c(), sasTriangle.getAngle_c(),
				DELTA);

	}

}
