package se.ericsson.cli;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.ericsson.model.Triangle;



public class CliInputTest{
	private CliInput cliInput;
	private String args1[] = {"-sideA", "7", "-sideB", "6", "-sideC", "5"};
	private String args2[] = {"-sideA", "3", "-sideB", "4", "-angleC", "67"};
	private static final double DELTA = 1e-8;
	
	@Before
	public void init() throws Exception {
		cliInput = new CliInput();
		
	}
	
	@After
	public void cleanup(){
		cliInput=null;
	}
	
	@Test
	public void testSSSTriangleInput(){
		Triangle triangle = cliInput.readInput(args1);
		assertEquals(7, triangle.getSide_a(), DELTA);
		assertEquals(6, triangle.getSide_b(), DELTA);
		assertEquals(5, triangle.getSide_c(), DELTA);
		
	}
	
	@Test
	public void testSASTriangleInput(){
		Triangle triangle = cliInput.readInput(args2);
		assertEquals(3, triangle.getSide_a(), DELTA);
		assertEquals(4, triangle.getSide_b(), DELTA);
		assertEquals(67, triangle.getAngle_c(), DELTA);
		
	}

}
