package se.ericsson.model.message;

import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import se.ericsson.model.Parameters;

/**
 * Message object is a subclass of the org.json.JSONObject and is used
 * supporting the JSON message format used in communication with RabbitMQ for
 * the RabbitMQ-based RPC triangle calculator
 * 
 * @author smitha.dabrai
 * 
 */
public class Message extends JSONObject implements Parameters {

	public static final String OUTPUT = "The computed triangle parameters for the given input are as follows: ";
	public static final String NEWLINE = "\n";
	

	/**
	 * Default constructor for Message
	 */
	public Message() {

	}

	/**
	 * Constructor that takes a JSONTokener
	 * 
	 * @param tokener
	 *            JSONTokener
	 */
	public Message(JSONTokener tokener) {
		super(tokener);
	}

	public double getSide_a() {
		return getDouble(SIDE_A);
	}

	public void setSide_a(double side_a) {
		put(SIDE_A, side_a);

	}

	public double getSide_b() {
		return getDouble(SIDE_B);
	}

	public void setSide_b(double side_b) {
		put(SIDE_B, side_b);

	}

	public double getSide_c() {
		return getDouble(SIDE_C);
	}

	public void setSide_c(double side_c) {
		put(SIDE_C, side_c);

	}

	public double getAngle_a() {
		return getDouble(ANGLE_A);
	}

	public void setAngle_a(double angle_a) {
		put(ANGLE_A, angle_a);

	}

	public double getAngle_b() {
		return getDouble(ANGLE_B);
	}

	public void setAngle_b(double angle_b) {
		put(ANGLE_B, angle_b);

	}

	@SuppressWarnings("finally")
	public double getAngle_c() {
		double d = 0;

		try {
			d = getDouble(ANGLE_C);
		} catch (JSONException e) {

		} finally {
			return d;
		}

	}

	public void setAngle_c(double angle_c) {
		put(ANGLE_C, angle_c);

	}

	public double getArea() {
		return getDouble(AREA);
	}

	public void setArea(double area) {
		put(AREA, area);

	}

	public double getPerimeter() {
		return getDouble(PERIMETER);
	}

	public void setPerimeter(double perimeter) {
		put(PERIMETER, perimeter);

	}

	/**
	 * 
	 * @return the byte array containing the JSON message
	 */
	public byte[] toByteArray() {
		return this.toString().getBytes();
	}

	/**
	 * 
	 * @param data
	 *            the byte array containing the JSON message
	 * @return Message the parsed JSOn Message
	 * @throws Exception
	 */
	public static Message parseFrom(byte[] data) throws Exception {
		String str = new String(data, "UTF-8");
		JSONTokener tokener = new JSONTokener(str);
		return new Message(tokener);
	}

	public boolean isSASTriangle() {
		return getBoolean(IS_SAS_TRIANGLE);
	}

	public void setSASTriangle(boolean isSASTriangle) {
		put(IS_SAS_TRIANGLE, isSASTriangle);

	}

	/**
	 * prints the formatted response to the console
	 */
	public String getFormattedOutput() {
		DecimalFormat df = new DecimalFormat("#,###,##0.000");
		StringBuffer sb = new StringBuffer();

		sb.append(OUTPUT);
		sb.append(NEWLINE);
		sb.append(Parameters.AREA + ": " + df.format(getArea()) + " " + SQ_UNITS);
		sb.append(NEWLINE);
		sb.append(Parameters.PERIMETER + ":" + df.format(getPerimeter())+ " "+UNITS);
		sb.append(NEWLINE);
		sb.append(Parameters.ANGLE_A + ": " + df.format(getAngle_a())+ " "+DEGREES);
		sb.append(NEWLINE);
		sb.append(Parameters.ANGLE_B + ": " + df.format(getAngle_b())+ " "+DEGREES);
		sb.append(NEWLINE);
		if (!isSASTriangle())
			sb.append(Parameters.ANGLE_C + ": " + df.format(getAngle_c())+" "+DEGREES);
		else {
			sb.append(Parameters.SIDE_C + ": " + df.format(getSide_c())+ " "+UNITS);
		}
		sb.append(NEWLINE);
		return sb.toString();
	}

}
