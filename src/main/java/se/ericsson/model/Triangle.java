package se.ericsson.model;

import se.ericsson.exception.InvalidInputException;

/**
 * This class is used as a model object for holding the parameters of a triangle
 * 
 * @author smitha.dabrai
 * 
 */
public class Triangle implements Parameters {
	private static final String INVALID_TRIANGLE_MSG = "No such triangle exists";
	private double side_a;
	private double side_b;
	private double side_c;
	private double angle_a;
	private double angle_b;
	private double angle_c;
	private double area;
	private double perimeter;
	private boolean isSASTriangle;

	/**
	 * Constructor with parameters
	 * 
	 * @param a
	 *            - indicates sideA of the Triangle
	 * @param b
	 *            - indicates sideB of the Triangle
	 * @param c
	 *            - indicates sideC/angleC of the Triangle based on the
	 *            parameter - isSASTriangle
	 * @param isSASTriangle
	 *            - indicates if the Triangle input parameters are in the form
	 *            of Side-Angle-Side
	 * @throws InvalidInputException
	 */
	public Triangle(double a, double b, double c, boolean isSASTriangle)
			throws InvalidInputException {

		if (isSASTriangle) {
			this.side_a = a;
			this.side_b = b;
			this.angle_c = c;

		} else {
			this.side_a = a;
			this.side_b = b;
			this.side_c = c;

		}
		this.isSASTriangle = isSASTriangle;
		if (!isValid()) {
			throw new InvalidInputException(INVALID_TRIANGLE_MSG);
		}

	}

	public double getSide_a() {
		return side_a;
	}

	public void setSide_a(double side_a) {
		this.side_a = side_a;
	}

	public double getSide_b() {
		return side_b;
	}

	public void setSide_b(double side_b) {
		this.side_b = side_b;
	}

	public double getSide_c() {
		return side_c;
	}

	public void setSide_c(double side_c) {
		this.side_c = side_c;
	}

	public double getAngle_a() {
		return angle_a;
	}

	public void setAngle_a(double angle_a) {
		this.angle_a = angle_a;
	}

	public double getAngle_b() {
		return angle_b;
	}

	public void setAngle_b(double angle_b) {
		this.angle_b = angle_b;
	}

	public double getAngle_c() {
		return angle_c;
	}

	public void setAngle_c(double angle_c) {
		this.angle_c = angle_c;
	}

	public double getArea() {
		return area;
	}

	/**
	 * Computes the area of the triangle. This method assumes that the perimeter
	 * is available.
	 */
	public void computeArea() {
		if (perimeter > 0) {
			double s = perimeter / 2;
			area = Math.sqrt(s * (s - side_a) * (s - side_b) * (s - side_c));
		}

	}

	public double getPerimeter() {
		return perimeter;
	}

	/**
	 * Computes the perimeter of the triangle. This method assumes that the 3
	 * sides of the triangle are available.
	 */

	public void computePerimeter() {
		if ((side_a > 0) && (side_b > 0) && (side_c > 0))
			this.perimeter = (side_a + side_b + side_c);
	}

	/**
	 * 
	 * @return the boolean value indicating if a valid Triangle can be created
	 *         with the given parameters
	 */
	public boolean isValid() {

		if (side_c != 0) {
			return (side_a > 0) && (side_b > 0) && (side_c > 0)
					&& (side_a + side_b > side_c) && (side_a + side_c > side_b)
					&& (side_c + side_b > side_a);
		}

		return ((side_a > 0) && (side_b > 0) && (angle_c > 0) && (angle_c < 180));
	}

	/**
	 * Computes the angle A that is the angle between side b and side c
	 */

	public void computeAngle_a() {

		this.angle_a = Math.toDegrees(Math.acos((side_b * side_b + side_c
				* side_c - side_a * side_a)
				/ (2.0 * side_b * side_c)));

	}

	/**
	 * Computes the angle B that is the angle between side a and side c
	 */
	public void computeAngle_b() {
		this.angle_b = Math.toDegrees(Math.acos((side_a * side_a + side_c
				* side_c - side_b * side_b)
				/ (2.0 * side_a * side_c)));
	}

	/**
	 * Computes the angle C that is the angle between side a and side b
	 */
	public void computeAngle_c() {
		this.angle_c = 180 - (angle_a + angle_b);
	}

	/**
	 * Computes the side c
	 */
	public void computeSide_c() {
		this.side_c = Math.sqrt(side_b * side_b + side_a * side_a
				- (2 * side_b * side_a * Math.cos(Math.toRadians(angle_c))));
	}

	/**
	 * Computes the remaining parameters of the Triangle. If the input is a
	 * Side-Side-Side triangle input then it computes all the remaining angles,
	 * area & perimeter. If the input is a Side-Angle-Side triangle input then
	 * it computes the 3 side, remaining angles, area & perimeter.
	 */
	public void computeRemainingTriangleParameters() {
		if (isSASTriangle)
			computeSide_c();

		computeAngle_a();
		computeAngle_b();
		if (!isSASTriangle)
			computeAngle_c();
		computePerimeter();
		computeArea();

	}

	public boolean isSASTriangle() {
		return isSASTriangle;
	}

	public void setSASTriangle(boolean isSASTriangle) {
		this.isSASTriangle = isSASTriangle;
	}

}
