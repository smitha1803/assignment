package se.ericsson.model;

/**
 * Interface for model
 * @author smitha.dabrai
 *
 */
public interface Parameters {
	public static final String SIDE_A = "sideA";
	public static final String SIDE_B = "sideB";
	public static final String SIDE_C = "sideC";
	public static final String ANGLE_A = "angleA";
	public static final String ANGLE_B = "angleB";
	public static final String ANGLE_C = "angleC";
	public static final String AREA = "area";
	public static final String PERIMETER = "perimeter";
	public static final String IS_SAS_TRIANGLE = "isSASTriangle";
	
	public static final String UNITS = "units";
	public static final String SQ_UNITS = "sq.units";
	public static final String DEGREES = "degrees";
	
	public double getSide_a();

	public void setSide_a(double side_a);

	public double getSide_b();

	public void setSide_b(double side_b);

	public double getSide_c();

	public void setSide_c(double side_c);

	public double getAngle_a();

	public void setAngle_a(double angle_a);

	public double getAngle_b();

	public void setAngle_b(double angle_b);

	public double getAngle_c();

	public void setAngle_c(double angle_c);

	public double getArea();
	
	public double getPerimeter();
	
	public boolean isSASTriangle();
	
	public void setSASTriangle(boolean isSASTriangle);

}
