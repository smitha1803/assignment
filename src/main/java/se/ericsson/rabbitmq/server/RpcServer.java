package se.ericsson.rabbitmq.server;

import java.io.IOException;

import se.ericsson.model.Triangle;
import se.ericsson.model.message.Message;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

/**
 * Server for RabbitMQ-based RPC triangle calculator. This class receives
 * triangle parameters (angles or length of sides) as JSON message from the RPC
 * client. It computes the parameters like perimeter, area & remaining the
 * remianing parameters of the triangle and sends the response back to the
 * calling client
 * 
 * @author smitha.dabrai
 * 
 */
public class RpcServer {

	private static final String DEFAULT = "Default";
	private static final String RPC_QUEUE_NAME = "rpc_queue";
	private static final int PREFETCH_COUNT = 1;
	private static final String HOST = "localhost";
	private Connection connection;
	private Channel channel;
	QueueingConsumer consumer;
	String name;

	/**
	 * Constructor that takes the String name
	 * 
	 * @param name
	 *            name of the server
	 * @throws IOException
	 */
	public RpcServer(String name) throws IOException {
		this.name = name;
	}

	/**
	 * Handles the initialisations
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		connection = factory.newConnection();
		channel = connection.createChannel();
		consumer = new QueueingConsumer(channel);
		System.out.println("Welcome to Server: [" + name + "]");
	}

	/**
	 * Waits for a request on the queue
	 * 
	 * @param queue
	 *            the queue that it listens on
	 * @throws IOException
	 */
	public void listen(String queue) throws IOException {
		channel.queueDeclare(queue, false, false, false, null);
		channel.basicQos(PREFETCH_COUNT);
		channel.basicConsume(queue, false, consumer);
		System.out.println(" [" + name + "] Awaiting RPC requests");

	}

	/**
	 * Processes the client request and provides the response after performing
	 * the computations for the triangle parameters
	 * 
	 * @throws Exception
	 */
	public void provideResponse() throws Exception {
		while (true) {

			Message response = null;

			QueueingConsumer.Delivery delivery = consumer.nextDelivery();

			BasicProperties props = delivery.getProperties();
			BasicProperties replyProps = new BasicProperties.Builder()
					.correlationId(props.getCorrelationId()).build();

			try {
				Message message = Message.parseFrom(delivery.getBody());
				Triangle triangle = toModel(message);
				triangle.computeRemainingTriangleParameters();
				response = createResponse(triangle);

			} catch (Exception e) {
				System.out.println(" [.] " + e.toString());
				response = null;
			} finally {
				channel.basicPublish("", props.getReplyTo(), replyProps,
						response.toByteArray());

				channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			}
		}
	}

	/**
	 * Converts the JSON message containing the input parameters to the Triangle
	 * model object
	 * 
	 * @param message
	 *            the JSON Message containing the input parameters as send by
	 *            the RPC client
	 * @return Triangle model object
	 * @throws Exception
	 */
	public Triangle toModel(Message message) throws Exception {
		System.out.println("[" + name + "] Received message("
				+ message.toString() + ")");
		double d = 0;
		boolean isSASTriangle = message.isSASTriangle();
		if (isSASTriangle) {
			d = message.getAngle_c();
			
		} else {
			d = message.getSide_c();
		}
		Triangle triangle = new Triangle(message.getSide_a(),
				message.getSide_b(), d, isSASTriangle);

		return triangle;
	}

	/**
	 * 
	 * Creates a RPC response that is in the form of the Message JSON object
	 * 
	 * @param triangle
	 *            the triangle with the computed parameters
	 * 
	 *
	 * @return Message - JSON message object
	 */
	public Message createResponse(Triangle triangle) {
		Message msg = new Message();
		boolean isSASTriangle = triangle.isSASTriangle();
		if (isSASTriangle) {
			msg.setSide_c(triangle.getSide_c());
		} else {
			msg.setAngle_c(triangle.getAngle_c());
		}
		msg.setSASTriangle(isSASTriangle);
		msg.setAngle_a(triangle.getAngle_a());
		msg.setAngle_b(triangle.getAngle_b());
		msg.setPerimeter(triangle.getPerimeter());
		msg.setArea(triangle.getArea());
		return msg;
	}

	/**
	 * Closes the connection
	 */
	public void close() {
		try {
			// avoid AlreadyClosedException
			if (connection != null && connection.isOpen()) {
				connection.close();
			}
		} catch (IOException e) {
		}

	}
	
	/**
	 * Main program of the RPC Server
	 * @param argv the argument argv[0] for taking the command line input for the name
	 */

	public static void main(String[] argv) {
		String name = DEFAULT;
		if (argv.length > 0)
			name = argv[0];

		RpcServer rpcServer = null;

		try {
			rpcServer = new RpcServer(name);
			rpcServer.init();
			rpcServer.listen(RPC_QUEUE_NAME);
			rpcServer.provideResponse();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rpcServer != null)
				rpcServer.close();
		}
	}

}