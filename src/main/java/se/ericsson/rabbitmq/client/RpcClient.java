package se.ericsson.rabbitmq.client;

import java.util.UUID;

import se.ericsson.cli.CliInput;
import se.ericsson.model.Triangle;
import se.ericsson.model.message.Message;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

/**
 * Client for RabbitMQ-based RPC triangle calculator. This class receives
 * triangle parameters (angles or length of sides) as command line arguments,
 * validates that such triangle is possible (exists) and then send those
 * parameters to remote server to calculate perimeter, area & remaining
 * parameters.
 * 
 * @author smitha.dabrai
 * 
 */
public class RpcClient {

	private Connection connection;
	private Channel channel;
	private String replyQueueName;
	private QueueingConsumer consumer;
	private static final String REQUEST_QUEUE_NAME = "rpc_queue";
	private static final String HOST = "localhost";

	/**
	 * Initializes the callback queue
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		connection = factory.newConnection();
		channel = connection.createChannel();
		replyQueueName = channel.queueDeclare().getQueue();
		consumer = new QueueingConsumer(channel);
		channel.basicConsume(replyQueueName, true, consumer);
	}

	/**
	 * Performs the call to the server
	 * 
	 * @param message
	 *            JSON message object containing the input parameters of a
	 *            Side-Side-Side or Side-Angle-Side triangle
	 * @return Message response that is received from the server containing the
	 *         area, perimeter & the remaining parameters of the triangle
	 * @throws Exception
	 */
	public Message call(Message message) throws Exception {
		Message response = null;
		String corrId = UUID.randomUUID().toString();

		BasicProperties props = new BasicProperties.Builder()
				.correlationId(corrId).replyTo(replyQueueName).build();

		channel.basicPublish("", REQUEST_QUEUE_NAME, props,
				message.toByteArray());

		while (true) {
			QueueingConsumer.Delivery delivery = consumer.nextDelivery();
			if (delivery.getProperties().getCorrelationId().equals(corrId)) {
				response = Message.parseFrom(delivery.getBody());
				break;
			}
		}

		return response;
	}

	/**
	 * Close connection
	 * 
	 * @throws Exception
	 */
	public void close() throws Exception {
		connection.close();
	}

	/**
	 * Creates a RPC request that is in the form of the Message JSON object
	 * 
	 * @param triangle
	 *            the triangle with the input parameters set
	 * @return Message - JSON message object
	 */
	public Message createRequest(Triangle triangle) {
		Message message = new Message();
		message.setSide_a(triangle.getSide_a());
		message.setSide_b(triangle.getSide_b());
		boolean isSASTriangle = triangle.isSASTriangle();
		if (isSASTriangle) {
			message.setAngle_c(triangle.getAngle_c());
		} else {
			message.setSide_c(triangle.getSide_c());
		}
		message.setSASTriangle(isSASTriangle);
		return message;
	}

	/**
	 * Main program of the RPC client
	 * 
	 * @param argv
	 *            the arguments for taking the command line input E.g -sideA a
	 *            -sideB b -sideC c or -sideA a -sideB b -angleC c [in this case
	 *            we assume that C is angle between a and b ]
	 */

	public static void main(String[] argv) {
		CliInput cliInput = new CliInput();
		Triangle triangle = cliInput.readInput(argv);
		RpcClient triangleRpc = null;
		Message response = null;
		try {
			triangleRpc = new RpcClient();
			triangleRpc.init();

			System.out.println(" [x] Requesting triangle program()");
			response = triangleRpc.call(triangleRpc.createRequest(triangle));
			System.out.println(response.getFormattedOutput());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (triangleRpc != null) {
				try {
					triangleRpc.close();
				} catch (Exception ignore) {
				}
			}
		}
	}
}
