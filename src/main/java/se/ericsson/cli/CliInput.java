package se.ericsson.cli;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

import se.ericsson.exception.InvalidInputException;
import se.ericsson.model.Triangle;

/**
 * This class is used for taking the command line input for the RpcClient
 * Program
 * 
 * @author smitha.dabrai
 * 
 */
public class CliInput {
	private static final String HELP_MSG = "The arguments for taking the command line input are : -sideA a -sideB b -sideC c [where a, b & c are the 3 sides of a triangle] or -sideA a -sideB b -angleC c [in this case we assume that C is angle between side a and side b]. The sides and/or angle should be non-negative numeric values";
	private static final String HELP_LONG_OPT = "help";
	private static final String HELP_OPT = "h";
	private static final String SIDE_A_LONG_OPT = "Side1 of a Triangle";
	private static final String SIDE_B_LONG_OPT = "Side2 of a Triangle";

	/**
	 * Returns the options for taking the command line input used for taking the
	 * parameters for Side-Side-Side Triangle or a Side Angle Side Triangle
	 * 
	 * @return the options created
	 */
	private Options createOptions() {
		Options options = new Options();
		options.addOption(HELP_OPT, HELP_LONG_OPT, false, HELP_MSG);
		options.addOption(Triangle.SIDE_A, true, SIDE_A_LONG_OPT);
		options.addOption(Triangle.SIDE_B, true, SIDE_B_LONG_OPT);

		OptionGroup optionGroup = new OptionGroup();
		optionGroup.addOption(OptionBuilder.hasArg(true)
				.create(Triangle.SIDE_C));
		optionGroup.addOption(OptionBuilder.hasArg(true).create(
				Triangle.ANGLE_C));
		options.addOptionGroup(optionGroup);
		return options;

	}

	/**
	 * 
	 * @param options
	 *            the options for taking the command line input
	 */
	private void showHelp(Options options) {
		HelpFormatter h = new HelpFormatter();
		h.printHelp(HELP_LONG_OPT, options);
		System.exit(-1);
	}

	/**
	 * 
	 * @param args
	 *            the arguments for taking the command line input E.g -sideA a
	 *            -sideB b -sideC c or -sideA a -sideB b -angleC c [in this case
	 *            we assume that C is angle between a and b ]
	 * @return a valid Triangle with the input parameters
	 */
	public Triangle readInput(String[] args) {
		Triangle triangle = null;
		Options options = createOptions();
		try {
			CommandLineParser parser = new BasicParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption(Triangle.ANGLE_C)) {
				double s1 = Double.parseDouble(cmd
						.getOptionValue(Triangle.SIDE_A));
				double s2 = Double.parseDouble(cmd
						.getOptionValue(Triangle.SIDE_B));
				double a3 = Double.parseDouble(cmd
						.getOptionValue(Triangle.ANGLE_C));
				triangle = new Triangle(s1, s2, a3, true);

			} else {
				double s1 = Double.parseDouble(cmd
						.getOptionValue(Triangle.SIDE_A));
				double s2 = Double.parseDouble(cmd
						.getOptionValue(Triangle.SIDE_B));
				double s3 = Double.parseDouble(cmd
						.getOptionValue(Triangle.SIDE_C));
				triangle = new Triangle(s1, s2, s3, false);
			}

		} catch (InvalidInputException ie) {
			System.out.println(ie.getMessage());
			System.exit(-1);
		}catch (NumberFormatException nfe) {
			System.out.println("Illegal Argument");
			showHelp(options);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			showHelp(options);
		}
		return triangle;
	}

}
