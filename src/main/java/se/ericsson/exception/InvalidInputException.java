package se.ericsson.exception;

/**
 * This class is a subclass of java.lang.Exception and is used when a user tries to enter 
 * the parameter for a Triangle that do not form a valid Triangle 
 * @author smitha.dabrai
 *
 */
public class InvalidInputException extends Exception {

	private static final long serialVersionUID = 6398569964403346435L;

	/**
	 * 
	 * @param message the message for the exception
	 */
	public InvalidInputException(String message) {
		super(message);

	}

}
